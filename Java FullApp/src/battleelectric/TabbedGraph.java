package battleelectric;

import com.sun.tools.visualvm.charts.*;
import java.awt.Dimension;
import javax.swing.*;
import java.awt.GridLayout;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TabbedGraph {

    private SimpleXYChartSupport DefinedChart = null;
    private String SoldierName = "";
    private AcceptData newclient;
    private boolean flag = true;
    private ArrayList<InputData> inputdata;
    private InterpretData table = null;

    public TabbedGraph(String name, ArrayList<InputData> inputdata, InterpretData table) {
        this.table = table;
        this.SoldierName = name;
        this.inputdata = inputdata;
    }

    public JPanel GraphPanel() {
        JPanel panel = new JPanel(false);
        panel.setLayout(new GridLayout(1, 1));
        CreateGraph();
        JComponent chart = DefinedChart.getChart();
        chart.setPreferredSize(new Dimension(700, 200));
        panel.add(chart, JComponent.CENTER_ALIGNMENT);
        return panel;
    }

    //create the real time graph
    private void CreateGraph() {
        SimpleXYChartDescriptor sxycd = SimpleXYChartDescriptor.decimal(0, 10, 10, 1d, false, 500);
        sxycd.addLineItems(SoldierName);
        sxycd.setXAxisDescription("Time");
        sxycd.setYAxisDescription("Voltage");
        //sxycd.setChartTitle(SoldierName);
        DefinedChart = ChartFactory.createSimpleXYChart(sxycd);
        newclient = new AcceptData();
        newclient.start();
    }

    public void StopServer() {
        this.flag = false;
    }

    private class AcceptData extends Thread {

        private ArrayList<Double> tempdata = null;

        public AcceptData() {
            this.tempdata = new ArrayList<>();
        }

        private double GetAverage() {
            double sum = 0;
            for (int i = 0; i < tempdata.size(); i++) {
                sum = sum + tempdata.get(i);
            }
            return sum / tempdata.size();
        }

        @Override
        public void run() {
            Socket connectionSocket = null;
            BufferedReader input = null;
            ServerSocket newSocket = null;
            long[] value = new long[1];
            long time = 0;
            String[] tempstring = null;
            long templong;
            long counter = 0;
            InputData data = null;
            try {
                newSocket = new ServerSocket(2503);
                connectionSocket = newSocket.accept();
                input = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                while (flag) {
                    String temp = "";
                    //time = System.currentTimeMillis();
                    try {
                        temp = input.readLine();
                        tempstring = temp.split(",");
                    } catch (IOException ex) {
                        Logger.getLogger(TabbedGraph.class.getName()).log(Level.SEVERE, null, ex);
                        System.err.println("readline problem");
                    }

                    if (!tempstring[tempstring.length - 1].equals("T")) {
                        for (int i = 0; i < tempstring.length; i++) {
                            templong = Long.parseLong(tempstring[i]);
                            value[0] = templong;
                            data = new InputData(counter, templong);
                            inputdata.add(data);
                            DefinedChart.addValues(counter, value);
                            counter++;
                        }
                    } else {
                        flag = false;
                    }

                    //display the average in the last 10 seconds
//                    if (Math.floor(this.tempdata.size() / 5) >= 5) {
//                        table.GetTable().getModel().setValueAt(GetAverage(), 1, 1);
//                        for (int i = 0; i < 5; i++) {
//                            tempdata.remove(0);
//                        }
//                    } else {
//                        this.tempdata.add((double) value[0]);
//                    }
                    //Updata current value
                    table.GetTable().getModel().setValueAt((value[0]), 1, 0);
                    Thread.sleep(1);
                }
                //clean up
                connectionSocket.close();
                if (input != null) {
                    input.close();
                }
                if (newSocket != null) {
                    newSocket.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(TabbedGraph.class.getName()).log(Level.SEVERE, null, ex);
                System.err.println("IOException");
            } catch (InterruptedException ex) {
                Logger.getLogger(TabbedGraph.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Interrupt");
            }
        }
    }
}
