package battleelectric;

import java.util.Date;

public class InputData {

    private long time;
    private long value;
    public InputData(long time, long value) {
        this.time = time;
        this.value = value;
    }

    public Date getDate() {
        return new Date(this.time);
    }

    public long getValue() {
        return this.value;
    }
    
    public long getTime(){
        return this.time;
    }
   
    
}
