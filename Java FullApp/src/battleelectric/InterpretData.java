/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package battleelectric;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Clint
 */
//JPanel that displays the Table
public class InterpretData extends JPanel {

    //private TableModel themodel = null;
    private JTable theTable = null;

    public InterpretData() {

        theTable = new JTable(new myTableModel());
        theTable.setEnabled(false);
        add(theTable);
    }

    public JTable GetTable() {
        return this.theTable;
    }

    public class myTableModel extends AbstractTableModel {

        private String[] columnNames = {"CurrentValue", "Average", "Status"};
        private String[] InitialData = {"N/A", "N/A", "N/A"};
        private Object[][] data = null;

        public myTableModel() {
            data = new Object[2][4];
            System.arraycopy(columnNames, 0, data[0], 0, columnNames.length);
            System.arraycopy(InitialData, 0, data[1], 0, InitialData.length);
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public int getRowCount() {
            return data.length;
        }

        @Override
        public String getColumnName(int col) {
            return columnNames[col];
        }

        @Override
        public Object getValueAt(int row, int col) {
            return data[row][col];
        }

        @Override
        public void setValueAt(Object value, int row, int col) {
            data[row][col] = value;
            fireTableCellUpdated(row, col);
        }
    }
}
