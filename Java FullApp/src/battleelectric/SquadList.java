package battleelectric;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;

//delete the squats and corresponding soldiers 
public class SquadList extends JPanel
        implements ListSelectionListener {

    private JList list;
    private DefaultListModel listModel;
    private static final String addString = "Add";
    private static final String deleteString = "Delete";
    private JButton deleteButton;
    private JTextField squadName;
    private String name;
    private SoldierListContainer soldierListContainer;

    public SquadList(String s, SoldierListContainer c) {
        super(new BorderLayout());
        name = s;
        soldierListContainer = c;
        listModel = new DefaultListModel();
        //Create the list and put it in a scroll pane.
        list = new JList(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);
        list.addListSelectionListener(this);
        list.setVisibleRowCount(9);
        JScrollPane listScrollPane = new JScrollPane(list);
        //Create Title for the Select Squad Porition
        TitledBorder empty = BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), name);
        Font font = new Font("DejaVu Sans", Font.BOLD, 16);
        empty.setTitleFont(font);
        empty.setTitlePosition(TitledBorder.CENTER);
        listScrollPane.setBorder(empty);

        JButton addButton = new JButton(addString);
        SquadList.AddListener hireListener = new SquadList.AddListener(addButton);
        addButton.setActionCommand(addString);
        addButton.addActionListener(hireListener);
        addButton.setEnabled(false);

        deleteButton = new JButton(deleteString);
        deleteButton.setActionCommand(deleteString);
        deleteButton.addActionListener(new SquadList.DeleteListener());

        squadName = new JTextField(10);
        squadName.addActionListener(hireListener);
        squadName.getDocument().addDocumentListener(hireListener);
        if (listModel.isEmpty() != true) {
            String name = listModel.getElementAt(
                    list.getSelectedIndex()).toString();
        }

        //Create a panel that uses BoxLayout.
        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new BoxLayout(buttonPane,
                BoxLayout.LINE_AXIS));
        buttonPane.add(deleteButton);
        buttonPane.add(Box.createHorizontalStrut(5));
        buttonPane.add(new JSeparator(SwingConstants.VERTICAL));
        buttonPane.add(Box.createHorizontalStrut(5));
        buttonPane.add(squadName);
        buttonPane.add(addButton);
        buttonPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        add(listScrollPane, BorderLayout.CENTER);
        add(buttonPane, BorderLayout.PAGE_END);
    }

    public int returnSquadSelected() {
        return list.getSelectedIndex();
    }

    class DeleteListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            //This method can be called only if
            //there's a valid selection
            //so go ahead and remove whatever's selected.
            int index = list.getSelectedIndex();
            if (index < 0) {
                return;
            }
            soldierListContainer.deleteSoldierList(index);
            listModel.remove(index);
            int size = listModel.getSize();

            if (size == 0) { //Nobody's left, disable firing.
                deleteButton.setEnabled(false);
                soldierListContainer.clearList();
            } else { //Select an index.
                if (index == listModel.getSize()) {
                    //removed item in last position
                    index--;
                }
                soldierListContainer.setSquadListSelectedSquadNumber(list.getSelectedIndex());
                list.setSelectedIndex(index);
                list.ensureIndexIsVisible(index);
                if (listModel.getSize() == 0) {
                    System.out.println("Clear");
                }
            }
        }
    }

    //This listener is shared by the text field and the hire button.
    class AddListener implements ActionListener, DocumentListener {

        private boolean alreadyEnabled = false;
        private JButton button;

        public AddListener(JButton button) {
            this.button = button;
        }

        //Required by ActionListener.
        public void actionPerformed(ActionEvent e) {
            String name = squadName.getText();

            //User didn't type in a unique name...
            if (name.equals("") || alreadyInList(name)) {
                Toolkit.getDefaultToolkit().beep();
                squadName.requestFocusInWindow();
                squadName.selectAll();
                return;
            }

            int index = list.getSelectedIndex(); //get selected index
            if (index == -1) { //no selection, so insert at beginning
                index = 0;
            } else {           //add after the selected item
                index++;
            }

            listModel.insertElementAt(squadName.getText(), index);
            //If we just wanted to add to the end, we'd do this:
            //listModel.addElement(squadName.getText());

            //Reset the text field.
            squadName.requestFocusInWindow();
            squadName.setText("");

            SoldierList tempSoldierList = new SoldierList(index);
            soldierListContainer.addSoldierList(tempSoldierList, index);
            soldierListContainer.setSquadListSelectedSquadNumber(list.getSelectedIndex());
            //Select the new item and make it visible.
            list.setSelectedIndex(index);
            list.ensureIndexIsVisible(index);

        }

        //This method tests for string equality
        protected boolean alreadyInList(String name) {
            return listModel.contains(name);
        }

        //Required by DocumentListener.
        public void insertUpdate(DocumentEvent e) {
            enableButton();
        }

        //Required by DocumentListener.
        public void removeUpdate(DocumentEvent e) {
            handleEmptyTextField(e);
        }

        //Required by DocumentListener.
        public void changedUpdate(DocumentEvent e) {
            if (!handleEmptyTextField(e)) {
                enableButton();
            }
        }

        private void enableButton() {
            if (!alreadyEnabled) {
                button.setEnabled(true);
            }
        }

        private boolean handleEmptyTextField(DocumentEvent e) {
            if (e.getDocument().getLength() <= 0) {
                button.setEnabled(false);
                alreadyEnabled = false;
                return true;
            }
            return false;
        }
    }

    //This method is required by ListSelectionListener.
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() == false) {

            if (list.getSelectedIndex() == -1) {
                //No selection, disable fire button.
                deleteButton.setEnabled(false);

            } else {
                //Selection, enable the fire button.
                deleteButton.setEnabled(true);
                soldierListContainer.updateSoldierList(list.getSelectedIndex());
                soldierListContainer.setSquadListSelectedSquadNumber(list.getSelectedIndex());
            }
        }
    }
}
