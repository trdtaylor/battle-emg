package battleelectric;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

public class SoldierListContainer extends JPanel {

    private JList list;
    private DefaultListModel listModel;
    private static final String addString = "Add";
    private static final String deleteString = "Delete";
    private JButton deleteButton;
    private JButton addButton;
    private JTextField squadName;
    private String name;
    private SoldierList[] soldierList;
    private int squadListSelectedSquadNumber;
    private JTabbedPane TabContainer;
    private JScrollPane Scroll;
    private AddListener addlistener;
    private JPanel buttonPane;
    private TabbedGraph newtab;
    private JMenu edit;
    private ArrayList<InputData> inputdata;

    public SoldierListContainer(String s, JTabbedPane pane, JMenu edit) {
        super(new BorderLayout());
        this.TabContainer = pane;
        this.edit = edit;
        soldierList = new SoldierList[100];
        listModel = new DefaultListModel();
        name = s;
        inputdata = new ArrayList();
        SetJlist();
        TabContainer.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent evt) {
                list.setSelectedIndex(TabContainer.getSelectedIndex());
            }
        });

        this.edit.addMenuListener(new EditListener());
        //Snapshot lisener
        this.edit.getItem(0).addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (!inputdata.isEmpty()) {
                    ImageIcon image = new ImageIcon(this.getClass().getResource("../Resource/closeTab.gif"));
                    JPanel backgroungpanel = new JPanel();
                    backgroungpanel.add(CreateSnapshot(inputdata));
                    TabContainer.addTab("Snapshot", null, backgroungpanel, "Does nothing");
                    TabContainer.setSelectedIndex(TabContainer.getTabCount()-1);
                    //TabContainer.setIconAt(1, image)
//                    JPanel foo = new JPanel();
//                    //foo.add(new JLabel("haha"));
//                    foo.add(new JButton("haha",image));
//                    TabContainer.setTabComponentAt(1, foo);
                } else {
                    warning("No data to capture.");
                }
            }
        });
        SetScrollPane();
        SetAdd();
        SetDelete();
        SetTextField();
        SetButtonPane();
        add(Scroll, BorderLayout.CENTER);
        add(buttonPane, BorderLayout.PAGE_END);
    }

    //create the snapshot of a second
    private ChartPanel CreateSnapshot(ArrayList<InputData> inputdata) {
        TimeSeries time = new TimeSeries("Status", Millisecond.class);
        for (int i = 0; i < inputdata.size(); i++) {
            time.addOrUpdate(new Millisecond(inputdata.get(i).getDate()), inputdata.get(i).getValue());
        }
        TimeSeriesCollection TimeCollection = new TimeSeriesCollection(time);
        JFreeChart chart = ChartFactory.createTimeSeriesChart(
                "Snapshot", // title
                "Time", // x-axis label
                "Current", // y-axis label
                TimeCollection, // data
                true, // create legend?
                true, // generate tooltips?
                false // generate URLs?
                );
        return new ChartPanel(chart);
    }

    //double click to open tabs
    private void openTabs(String name) {
        
        if (TabContainer.getTabCount() >= 1) {
            //pop up a dialog
            warning("Currently only support one soldier at a time");
        } else {
            JPanel TabBackground = new JPanel();
            TabBackground.setLayout(new BoxLayout(TabBackground, BoxLayout.PAGE_AXIS));
            TabBackground.add(Box.createRigidArea(new Dimension(200, 50)));
            //data table
            InterpretData table = new InterpretData();
            //pass in graph's name, data container, TablePanel
            newtab = new TabbedGraph(name,inputdata,table);
            //add the table and graph to the GUI
            TabBackground.add(table);
            TabBackground.add(newtab.GraphPanel());
            TabContainer.addTab(name, null, TabBackground, "Does nothing");
            TabContainer.setMnemonicAt(0, KeyEvent.VK_1);
            //The following line enables to use scrolling tabs.
            TabContainer.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        }
    }

    private void SetButtonPane() {
        buttonPane = new JPanel();
        buttonPane.setLayout(new BoxLayout(buttonPane,
                BoxLayout.LINE_AXIS));
        buttonPane.add(deleteButton);
        buttonPane.add(Box.createHorizontalStrut(5));
        buttonPane.add(new JSeparator(SwingConstants.VERTICAL));
        buttonPane.add(Box.createHorizontalStrut(5));
        buttonPane.add(squadName);
        buttonPane.add(addButton);
        buttonPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    }

    private void SetJlist() {
        list = new JList(listModel);
        list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        list.setSelectedIndex(0);
        list.setVisibleRowCount(9);
        //double click to open up a graph
        list.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int index = list.locationToIndex(e.getPoint());
                    openTabs((String) listModel.getElementAt(index) + "'s raw stream");
                    edit.getItem(0).setEnabled(true);
                }
            }
        });

    }

    private void SetTextField() {
        squadName = new JTextField(10);
        squadName.addActionListener(addlistener);
        squadName.getDocument().addDocumentListener(addlistener);
    }

    private void SetScrollPane() {
        Scroll = new JScrollPane(list);
        //Create Title for the Select Squad Porition
        TitledBorder empty = BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), name);
        Font font = new Font("DejaVu Sans", Font.BOLD, 16);
        empty.setTitleFont(font);
        empty.setTitlePosition(TitledBorder.CENTER);
        Scroll.setBorder(empty);
    }

    private void SetAdd() {
        addButton = new JButton(addString);
        addlistener = new SoldierListContainer.AddListener(addButton);
        addButton.setActionCommand(addString);
        addButton.addActionListener(addlistener);
        addButton.setEnabled(false);
    }

    private void SetDelete() {
        deleteButton = new JButton(deleteString);
        deleteButton.setActionCommand(deleteString);
        deleteButton.addActionListener(new SoldierListContainer.DeleteListener());
    }

    public void updateSoldierList(int squad) {
        listModel.clear();
        if (soldierList[squad] != null && soldierList[squad].getListModel().getSize() > 0) {
            for (int i = 0; i < soldierList[squad].getListModel().getSize(); i++) {
                listModel.add(i, soldierList[squad].getListModel().elementAt(i));
            }
        }
        list.repaint();
        list.validate();
    }

    public void clearList() {
        listModel.clear();
        list.repaint();
        list.validate();
    }

    public void addSoldierList(SoldierList soldiers, int index) {
        soldierList[index] = soldiers;
        updateSoldierList(index);
    }

    //delete the corresponding graph
    public void deleteSoldierList(int index) {
        String TabName = "";
        String soldiername = "";
        ListModel listmodel = soldierList[index].getListModel();
        int listsize = soldierList[index].getListModel().getSize();
        if (TabContainer.getTabCount() > 0) {
            TabName = TabContainer.getTitleAt(0).substring(0, TabContainer.getTitleAt(0).indexOf("'"));
        }
        for (int i = 0; i < listsize; i++) {
            soldiername = (String) listmodel.getElementAt(i);
            if (TabName.equals(soldiername)) {
                TabContainer.removeAll();
                edit.getItem(0).setEnabled(false);
                newtab.StopServer();
                break;
            }
        }

        for (int i = index; i < 99; i++) {
            if (soldierList[i] != null) {
                soldierList[i] = soldierList[i + 1];
            }
        }
        updateSoldierList(index);
    }

    //specify which squat those soldiers belong to
    public void setSquadListSelectedSquadNumber(int index) {
        squadListSelectedSquadNumber = index;
    }

    private void warning(String msg) {
        JDialog warning = new JDialog(new JFrame(), "Error", true);
        JPanel message = new JPanel();
        message.add(new JLabel(msg));
        JPanel okButton = new JPanel();
        JButton ok = new JButton("OK");
        ok.addActionListener(new SoldierListContainer.ButtonListener(warning));
        okButton.add(ok);
        warning.setSize(200, 300);
        warning.setLocationRelativeTo(null);
        warning.add(message);
        warning.add(okButton, BorderLayout.SOUTH);
        warning.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        warning.pack();
        warning.setVisible(true);
    }

    class ButtonListener implements ActionListener {

        private JDialog warning = null;

        public ButtonListener(JDialog warning) {
            this.warning = warning;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.warning.setVisible(false);
            this.warning.dispose();
        }
    }

    class EditListener implements MenuListener {

        @Override
        public void menuSelected(MenuEvent me) {
            //disable the interpret button if no graph presents
            if (TabContainer.getTabCount() == 0) {
                edit.getItem(0).setEnabled(false);
            }
        }

        @Override
        public void menuDeselected(MenuEvent me) {
            //do nothing
        }

        @Override
        public void menuCanceled(MenuEvent me) {
            //do nothing
        }
    }

    class DeleteListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            //This method can be called only if
            //there's a valid selection
            //so go ahead and remove whatever's selected.                
            String TabName = "";
            int index = list.getSelectedIndex();
            if (index < 0 || listModel.getSize() == 0) {
                return;
            }

            if (TabContainer.getTabCount() > 0) {
                TabName = TabContainer.getTitleAt(0).substring(0, TabContainer.getTitleAt(0).indexOf("'"));
            }
            String ClickedOnName = (String) soldierList[squadListSelectedSquadNumber].getListModel().getElementAt(index);
            //if the solider is being displayed
            if (TabName.equals(ClickedOnName)) {
                TabContainer.removeAll();
                newtab.StopServer();
                listModel.remove(index);
                soldierList[squadListSelectedSquadNumber].deleteSoldier(index);
                edit.getItem(0).setEnabled(false);
            } else {
                listModel.remove(index);
                soldierList[squadListSelectedSquadNumber].deleteSoldier(index);
            }
            index--;
            list.setSelectedIndex(index);
            list.ensureIndexIsVisible(index);
        }
    }

    //This listener is shared by the text field and the hire button.
    class AddListener implements ActionListener, DocumentListener {

        private boolean alreadyEnabled = false;
        private JButton button;

        public AddListener(JButton button) {
            this.button = button;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String name = squadName.getText();
            //User didn't type in a unique name...
            if (name.equals("") || alreadyInList(name) || soldierList[0] == null) {
                Toolkit.getDefaultToolkit().beep();
                squadName.requestFocusInWindow();
                squadName.selectAll();
                return;
            }
            int index = list.getSelectedIndex(); //get selected index
            if (index == -1) { //no selection, so insert at beginning
                index = 0;
            } else {           //add after the selected item
                index++;
            }
            //get multiple squats working
            listModel.insertElementAt(squadName.getText(), index);
            soldierList[squadListSelectedSquadNumber].addSoldier(squadName.getText());
            //Reset the text field.
            squadName.requestFocusInWindow();
            squadName.setText("");

            //Select the new item and make it visible.
            list.setSelectedIndex(index);
            list.ensureIndexIsVisible(index);
        }

        //This method tests for string equality. You could certainly
        //get more sophisticated about the algorithm.  For example,
        //you might want to ignore white space and capitalization.
        protected boolean alreadyInList(String name) {
            return listModel.contains(name);
            //you might want to ignore white space and capitalization.
        }

        //Required by DocumentListener.
        @Override
        public void insertUpdate(DocumentEvent e) {
            enableButton();
        }

        //Required by DocumentListener.
        @Override
        public void removeUpdate(DocumentEvent e) {
            handleEmptyTextField(e);
        }

        //Required by DocumentListener.
        @Override
        public void changedUpdate(DocumentEvent e) {
            if (!handleEmptyTextField(e)) {
                enableButton();
            }
        }

        private void enableButton() {
            if (!alreadyEnabled) {
                button.setEnabled(true);
            }
        }

        private boolean handleEmptyTextField(DocumentEvent e) {
            if (e.getDocument().getLength() <= 0) {
                button.setEnabled(false);
                alreadyEnabled = false;
                return true;
            }
            return false;
        }
    }
}
