package battleelectric;

import java.awt.*;
import javax.swing.*;

public class GridBagLayoutDemo {

    final static boolean shouldFill = true;
    final static boolean shouldWeightX = true;
    final static boolean RIGHT_TO_LEFT = false;
    private Container pane;
    private JTabbedPane tabbedpane = null;
    private JMenu edit = null;

    public GridBagLayoutDemo(Container JframePane, JMenu edit) {
        this.pane = JframePane;
        this.edit = edit;
        if (RIGHT_TO_LEFT) {
            pane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        }
        pane.setLayout(new GridBagLayout());
        //set up the layout
        CreateSquat(CreateSoldier(CreateTabs()));
    }

    public SoldierListContainer CreateSoldier(JTabbedPane tabpane) {
        SoldierListContainer soldierListContainer = new SoldierListContainer("Select Soldier", tabpane, edit);
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.weighty = 0.3;
        c.gridx = 1;
        c.gridy = 1;
        pane.add(soldierListContainer, c);
        return soldierListContainer;
    }

    public void CreateSquat(SoldierListContainer soldierListContainer) {
        JComponent squadList = new SquadList("Select Squad", soldierListContainer);
        squadList.setOpaque(true);
        GridBagConstraints c = new GridBagConstraints();
        if (shouldFill) {
            //natural height, maximum width
            c.fill = GridBagConstraints.HORIZONTAL;
        }
        if (shouldWeightX) {
            c.weightx = 0.5;
        }
        c.weighty = 0.3;
        c.gridx = 0;
        c.gridy = 1;
        pane.add(squadList, c);
    }

    public JTabbedPane CreateTabs() {
        tabbedpane = new JTabbedPane();
        //JPane backgroundpane = new JPane();
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.PAGE_START;
        c.fill = GridBagConstraints.BOTH;
        //c.ipady = 300;      //make this component tall
        c.weightx = 0.0;
        c.weighty = 0.5;
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = 0;;
        pane.add(tabbedpane, c);
        return tabbedpane;
    }

    public JTabbedPane getTabbedPane() {
        return tabbedpane;
    }

//    public static void statusToGreen() {
//        button.setBackground(Color.green);
//        String s = button.getText();
//        button.setText("<html>Active Soldier is: " + soldierName + "<br/>He is: " + " OK </html>");
//    }
//
//    public static void statusToRed() {
//        button.setBackground(Color.red);
//        String s = button.getText();
//        button.setText("<html>Active Soldier is: " + soldierName + "<br/>He is: " + " Dead </html>");
//    }
//
//    public static void statusToYellow() {
//        button.setBackground(Color.yellow);
//        String s = button.getText();
//        button.setText("<html>Active Soldier is: " + soldierName + "<br/>He is: " + " Under Stress </html>");
//    }
//
//    public static void statusToMissing() {
//        button.setBackground(Color.black);
//        String s = button.getText();
//        button.setText("<html>Active Soldier is: " + soldierName + "<br/>He is: " + " Missing </html>");
//    }
//
//    public static void test() {
//        button.setText("soldier1");
//    }
    public static void updateActiveSoldier(String name) {
        // tabs.add();
//        c.fill = GridBagConstraints.PAGE_START;
//        c.fill = GridBagConstraints.BOTH;
//	//c.ipady = 300;      //make this component tall
//	c.weightx = 0.0;
//        c.weighty = 0.5;
//	c.gridwidth = 3;
//	c.gridx = 0;
//	c.gridy = 0;
//	pane2.add(tabs, c);
//        if (name.equals("No Active Soldier")) {
//            button.setText("-------------No Active Soldier------------");
//        } else {
//            //button.setText("Active Soldier is: " + name);
//            soldierName = name;
//            button.setBackground(Color.green);
//            String s = button.getText();
//            button.setText("<html>Active Soldier is: " + soldierName + "<br/>He is: " + " OK </html>");
//        }
    }
}
