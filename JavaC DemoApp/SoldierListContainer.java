/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

/**
 *
 * @author Clayton
 */
/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;

/* ListDemo.java requires no other files. */
public class SoldierListContainer extends JPanel
                      implements ListSelectionListener {
    private JList list;
    private DefaultListModel listModel;

    private static final String addString = "Add";
    private static final String deleteString = "Delete";
    private JButton deleteButton;
    private JTextField squadName;
    private String name;
    private SoldierList[] soldierList;
    private int squadListSelectedSquadNumber;

    public SoldierListContainer(String s) {
        super(new BorderLayout());
        name = s;
        soldierList = new SoldierList[100];        
        listModel = new DefaultListModel();
     //   soldierList[0] = new SoldierList(1);
    //    listModel.addElement("Squad 1");

        //Create the list and put it in a scroll pane.
        list = new JList(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);
        list.addListSelectionListener(this);
        list.setVisibleRowCount(9);
        JScrollPane listScrollPane = new JScrollPane(list);
        //Create Title for the Select Squad Porition
        TitledBorder empty = BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), name);
        Font font = new Font("DejaVu Sans", Font.BOLD, 16);
        empty.setTitleFont(font);
        empty.setTitlePosition(TitledBorder.CENTER);
        listScrollPane.setBorder(empty);

        JButton addButton = new JButton(addString);
        SoldierListContainer.AddListener hireListener = new SoldierListContainer.AddListener(addButton);
        addButton.setActionCommand(addString);
        addButton.addActionListener(hireListener);
        addButton.setEnabled(false);

        deleteButton = new JButton(deleteString);
        deleteButton.setActionCommand(deleteString);
        deleteButton.addActionListener(new SoldierListContainer.DeleteListener());

        squadName = new JTextField(10);
        squadName.addActionListener(hireListener);
        squadName.getDocument().addDocumentListener(hireListener);
        if(listModel.isEmpty() != true) {
            String name = listModel.getElementAt(
                              list.getSelectedIndex()).toString();
        }

        //Create a panel that uses BoxLayout.
        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new BoxLayout(buttonPane,
                                           BoxLayout.LINE_AXIS));
        buttonPane.add(deleteButton);
        buttonPane.add(Box.createHorizontalStrut(5));
        buttonPane.add(new JSeparator(SwingConstants.VERTICAL));
        buttonPane.add(Box.createHorizontalStrut(5));
        buttonPane.add(squadName);
        buttonPane.add(addButton);

        buttonPane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

        add(listScrollPane, BorderLayout.CENTER);
        add(buttonPane, BorderLayout.PAGE_END);
    }
    public void updateSoldierList(int squad) {
        listModel.clear();
        if(soldierList[squad] != null && soldierList[squad].getListModel().getSize() > 0) {
            for(int i=0;i<soldierList[squad].getListModel().getSize();i++) {
                 listModel.add(i,soldierList[squad].getListModel().elementAt(i));
            }        
        }
        list.repaint();
        list.validate();
    }
    public void clearList() {
        listModel.clear();
        list.repaint();
        list.validate();
    }
    public void addSoldier(String name, int squad) {
        soldierList[squad].getListModel().addElement(name);
    }
    public void addSoldierList(SoldierList soldiers, int index) {
        soldierList[index] = soldiers;
        updateSoldierList(index);
    }
    public void deleteSoldierList(int index) {
        for(int i = index;i<99;i++) {
            soldierList[i] = soldierList[i+1];
        }
        //soldierList[index] = null;
       //     soldierList[index].getListModel().removeAllElements();

        
        updateSoldierList(index);
    }
    public void setSquadListSelectedSquadNumber(int index) {
        squadListSelectedSquadNumber = index;
    }

    class DeleteListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //This method can be called only if
            //there's a valid selection
            //so go ahead and remove whatever's selected.
            int index = list.getSelectedIndex();
            listModel.remove(index);
            soldierList[squadListSelectedSquadNumber].deleteSoldier(index);
            

            int size = listModel.getSize();

            if (size == 0) { //Nobody's left, disable firing.
                deleteButton.setEnabled(false);

            } else { //Select an index.
                if (index == listModel.getSize()) {
                    //removed item in last position
                    index--;
                }

                list.setSelectedIndex(index);
                list.ensureIndexIsVisible(index);
            }
        }
    }

    //This listener is shared by the text field and the hire button.
    class AddListener implements ActionListener, DocumentListener {
        private boolean alreadyEnabled = false;
        private JButton button;

        public AddListener(JButton button) {
            this.button = button;
        }

        //Required by ActionListener.
        public void actionPerformed(ActionEvent e) {
            String name = squadName.getText();

            //User didn't type in a unique name...
            if (name.equals("") || alreadyInList(name) || soldierList[0] == null) {
                Toolkit.getDefaultToolkit().beep();
                squadName.requestFocusInWindow();
                squadName.selectAll();
                return;
            }

            int index = list.getSelectedIndex(); //get selected index
            if (index == -1) { //no selection, so insert at beginning
                index = 0;
            } else {           //add after the selected item
                index++;
            }

            listModel.insertElementAt(squadName.getText(), index);
            soldierList[squadListSelectedSquadNumber].addSoldier(squadName.getText());
            //System.out.println(squadListSelectedSquadNumber);
            //If we just wanted to add to the end, we'd do this:
            //listModel.addElement(squadName.getText());

            //Reset the text field.
            squadName.requestFocusInWindow();
            squadName.setText("");

            //Select the new item and make it visible.
            list.setSelectedIndex(index);
            list.ensureIndexIsVisible(index);
        }

        //This method tests for string equality. You could certainly
        //get more sophisticated about the algorithm.  For example,
        //you might want to ignore white space and capitalization.
        protected boolean alreadyInList(String name) {
            return listModel.contains(name);
        }

        //Required by DocumentListener.
        public void insertUpdate(DocumentEvent e) {
            enableButton();
        }

        //Required by DocumentListener.
        public void removeUpdate(DocumentEvent e) {
            handleEmptyTextField(e);
        }

        //Required by DocumentListener.
        public void changedUpdate(DocumentEvent e) {
            if (!handleEmptyTextField(e)) {
                enableButton();
            }
        }

        private void enableButton() {
            if (!alreadyEnabled) {
                button.setEnabled(true);
            }
        }

        private boolean handleEmptyTextField(DocumentEvent e) {
            if (e.getDocument().getLength() <= 0) {
                button.setEnabled(false);
                alreadyEnabled = false;
                return true;
            }
            return false;
        }
    }

    //This method is required by ListSelectionListener.
    @Override
    public void valueChanged(ListSelectionEvent e) {
        
        if (e.getValueIsAdjusting() == false) {
            String s = listModel.get(list.getSelectedIndex()).toString();
            GridBagLayoutDemo.updateActiveSoldier(s);
            if (list.getSelectedIndex() == -1) {
            //No selection, disable fire button.
                deleteButton.setEnabled(false);

            } else {
            //Selection, enable the fire button.
                deleteButton.setEnabled(true);
            }
        }
    }
}

