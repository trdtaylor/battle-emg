/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

/**
 *
 * @author Clayton
 */
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
/** 
	This class demonstrates the basics of setting up a Java Swing GUI uisng the
	BorderLayout. You should be able to use this program to drop in other
	components when building a GUI 
*/
public class BasicGui{
	// Initialize all swing objects.
    private JFrame f = new JFrame("Basic GUI"); //create Frame
    private JPanel pnlCenter = new JPanel(); // Center quadrant

	// Buttons some there is something to put in the panels
    private JButton btnCenter = new JButton("Center");

    // Menu
    private JMenuBar mb = new JMenuBar(); // Menubar
    private JMenu mnuFile = new JMenu("File"); // File Entry on Menu bar
    private JMenu mnuEdit = new JMenu("Edit"); // Edit Entry on Menu bar
    private JMenuItem mnuItemSave = new JMenuItem("Save"); // Save sub item
    private JMenuItem mnuItemLoad = new JMenuItem("Load"); // Load sub item
    private JMenuItem mnuItemReset = new JMenuItem("Reset"); // Reset sub item
    private JMenuItem mnuItemQuit = new JMenuItem("Quit"); // Quit sub item
    private JMenu mnuHelp = new JMenu("Help"); // Help Menu entry
    private JMenuItem mnuItemAbout = new JMenuItem("About"); // About Entry

    /** Constructor for the GUI */
    public BasicGui(){
		// Set menubar
        f.setJMenuBar(mb);
        
		//Build Menus
	mnuFile.add(mnuItemSave);  //create save line
	mnuFile.add(mnuItemLoad);  //Create Load line
	mnuFile.add(mnuItemReset);  //Create Reset line
	mnuFile.add(mnuItemQuit);  // Create Quit line
        mnuHelp.add(mnuItemAbout); // Create About line
        mb.add(mnuFile);        // Add Menu items to form
	mb.add(mnuEdit);
        mb.add(mnuHelp);

        // Add Buttons
        pnlCenter.add(btnCenter);
        
        // Setup Main Frame
        
		// Allows the Swing App to be closed
        f.addWindowListener(new ListenCloseWdw());
		
		//Add Menu listener
        mnuItemQuit.addActionListener(new ListenMenuQuit());
    }
	
    public class ListenMenuQuit implements ActionListener{
        public void actionPerformed(ActionEvent e){
            System.exit(0);         
        }
    }
	
    public class ListenCloseWdw extends WindowAdapter{
        public void windowClosing(WindowEvent e){
            System.exit(0);         
        }
    }
	
    public void launchFrame(){
        // Display Frame
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	//f.add(new TabbedPaneDemo(), BorderLayout.PAGE_START);
        
        f.pack(); //Adjusts panel to components for display
        f.setVisible(true);
	f.setExtendedState(f.getExtendedState() | Frame.MAXIMIZED_BOTH);

    }
        public void createAndShowGUI() {
        //Create and set up the content pane.
       // JComponent newContentPane = new SquadList();

        //newContentPane.setOpaque(true); //content panes must be opaque
        
       // f.add(newContentPane, BorderLayout.EAST);
        GridBagLayoutDemo.addComponentsToPane(f.getContentPane());
    }
    
    public static void main(String args[]){
        BasicGui gui = new BasicGui();
        gui.createAndShowGUI();
        gui.launchFrame();
        

        
    }
}




