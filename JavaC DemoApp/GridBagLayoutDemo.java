/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

package javaapplication2;

/*
 * GridBagLayoutDemo.java requires no other files.
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GridBagLayoutDemo {
    final static boolean shouldFill = true;
    final static boolean shouldWeightX = true;
    final static boolean RIGHT_TO_LEFT = false;
    private static JButton button;
    private static JButton button2;
    private static JButton button9;
    private static JButton button3;
    private static JButton button4;
    private static JButton button5;
    private static JButton button6;
    private static JButton button7;
    private static JButton button8;
    private static GridBagConstraints c;
    private static GridBagConstraints d;
    private static JPanel panel;
    private static String soldierName;

    public static void addComponentsToPane(Container pane) {
        if (RIGHT_TO_LEFT) {
            pane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        }

        
	pane.setLayout(new GridBagLayout());
	c = new GridBagConstraints();
        
        
        SoldierListContainer soldierListContainer = new SoldierListContainer("Select Soldier");
	c.fill = GridBagConstraints.HORIZONTAL;
	c.weightx = 0.5;
        c.weighty = 0.3;
	c.gridx = 1;
	c.gridy = 1;
	pane.add(soldierListContainer, c);

//	button = new JButton(" for Status Colors");
        panel = new JPanel(new GridBagLayout());
        d = new GridBagConstraints();
       // createButtons();
     //  button = new JButton("<html>Click Me<br/>If You Dare</html>");
        button = new JButton("-------------No Active Soldier------------");
        button.setPreferredSize(new Dimension(70, 100));

        //button.setText("soldier1");
        button.setBackground(Color.green);
	c.fill = GridBagConstraints.HORIZONTAL;
	c.weightx = 0.5;
        c.weighty = 0.3;
	c.gridx = 2;
	c.gridy = 1;
	pane.add(button, c);
        
        
	c.fill = GridBagConstraints.HORIZONTAL;
	c.weightx = 0.5;
        c.weighty = 0.3;
	c.gridx = 2;
	c.gridy = 1;
        pane.add(panel,c);
        
        
        
        
        JComponent squadList = new SquadList("Select Squad",soldierListContainer);
        squadList.setOpaque(true);
	if (shouldFill) {
	//natural height, maximum width
	c.fill = GridBagConstraints.HORIZONTAL;
	}
	if (shouldWeightX) {
	c.weightx = 0.5;
	}
        //c.ipady = 100;
        c.weighty = 0.3;
	c.gridx = 0;
	c.gridy = 1;
	pane.add(squadList, c);


        
        //create tabs
        TabbedPaneDemo tabs = new TabbedPaneDemo();

	c.fill = GridBagConstraints.PAGE_START;
        c.fill = GridBagConstraints.BOTH;
	//c.ipady = 300;      //make this component tall
	c.weightx = 0.0;
        c.weighty = 0.5;
	c.gridwidth = 3;
	c.gridx = 0;
	c.gridy = 0;
	pane.add(tabs, c);
        
     /*   
	button = new JButton("5");
        
	c.fill = GridBagConstraints.HORIZONTAL;
	c.ipady = 0;       //reset to default
	c.weighty = 1.0;   //request any extra vertical space
	c.anchor = GridBagConstraints.PAGE_END; //bottom of space
	c.insets = new Insets(10,0,0,0);  //top padding
	c.gridx = 1;       //aligned with button 2
	c.gridwidth = 2;   //2 columns wide
	c.gridy = 2;       //third row
	pane.add(button, c);
        
        */
    }
    public static void createButtons(){
        
        button2= new JButton("Soldier2");
        button3 = new JButton("Soldier3");
        button4 = new JButton("Soldier4");
        button5 = new JButton("Soldier5");
        button6 = new JButton("Soldier6");
        button7 = new JButton("Soldier7");
        button8 = new JButton("Soldier8");
        button9 = new JButton("Soldier9");
        
        d.gridy=0;
        d.weightx = 1.0;
        d.fill = GridBagConstraints.HORIZONTAL;
        panel.add(button,d);
     //   d.fill = GridBagConstraints.HORIZONTAL;
        d.gridy=1;
    //    d.weightx = 1.0;
        panel.add(button4,d);
        d.gridy=2;
   //     d.fill = GridBagConstraints.HORIZONTAL;
        panel.add(button3,d);
  //      d.weightx = 1.0;
    //    d.fill = GridBagConstraints.HORIZONTAL;
        d.gridy=3;
        button4.setBackground(Color.red);
        panel.add(button4,d);
  //      d.weightx = 1.0;
 //       d.fill = GridBagConstraints.HORIZONTAL;
        d.gridy=4;
//        d.weightx = 1.0;
  //      d.fill = GridBagConstraints.HORIZONTAL;
        panel.add(button5,d);
        d.gridy=5;
        panel.add(button6,d);
        d.gridy=6;
        panel.add(button7,d);
        d.gridy=7;
        panel.add(button8,d);
        d.gridy=8;
        panel.add(button9,d);
    }
    public static void statusToGreen()
    {
        button.setBackground(Color.green);
        String s = button.getText();
        button.setText("<html>Active Soldier is: " + soldierName + "<br/>He is: " + " OK </html>");
    }
    public static void statusToRed()
    {
        button.setBackground(Color.red);
        String s = button.getText();
        button.setText("<html>Active Soldier is: " + soldierName + "<br/>He is: " + " Dead </html>");
    }
    public static void statusToYellow()
    {
        button.setBackground(Color.yellow);
        String s = button.getText();
        button.setText("<html>Active Soldier is: " + soldierName + "<br/>He is: " + " Under Stress </html>");
    }
    public static void statusToMissing()
    {
        button.setBackground(Color.black);
        String s = button.getText();
        button.setText("<html>Active Soldier is: " + soldierName + "<br/>He is: " + " Missing </html>");
    }
    public static void test()
    {
        button.setText("soldier1");
    }
    public static void updateActiveSoldier(String name)
    {
        button.setText("Active Soldier is: " + name);
        soldierName = name;
        button.setBackground(Color.green);
        String s = button.getText();
        button.setText("<html>Active Soldier is: " + soldierName + "<br/>He is: " + " OK </html>");
    }
}
