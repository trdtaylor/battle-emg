package javaapplication2;

/**
 *
 * @author Clayton
 */
import com.sun.tools.visualvm.charts.*;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.swing.JComponent;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TabbedPaneDemo extends JPanel {

    private SimpleXYChartSupport DefinedChart = null;

    public TabbedPaneDemo() {
        super(new GridLayout(1, 1));
        JTabbedPane tabbedPane = new JTabbedPane();
        //ImageIcon icon = createImageIcon("");
        JComponent panel1 = makeTextPanel1("EKG Graph Goes Here");
        tabbedPane.addTab("Raw Stream", null, panel1,
                "Does nothing");
        tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

//        JComponent panel2 = makeTextPanel2("Interpreted Stream Goes Here");
//        tabbedPane.addTab("Interpreted Stream", null, panel2,
//                "Does twice as much nothing");
//        tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);
        //Add the tabbed pane to this panel.
        add(tabbedPane);
        //The following line enables to use scrolling tabs.
        tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
    }

    protected JComponent makeTextPanel1(String text) {
        JPanel panel = new JPanel(false);
        panel.setLayout(new GridLayout(1, 1));
        CreateModel();
        panel.add(DefinedChart.getChart(), JComponent.CENTER_ALIGNMENT);
        return panel;
    }

    private void CreateModel() {
        SimpleXYChartDescriptor sxycd = SimpleXYChartDescriptor.decimal(0, 10, 10, 1d, false, 500);
        sxycd.addLineItems("Test");
        sxycd.setXAxisDescription("Time");
        sxycd.setYAxisDescription("Current");
        sxycd.setChartTitle("Real-Time Values");
        DefinedChart = ChartFactory.createSimpleXYChart(sxycd);
        new Generator().start();
    }

    private class Generator extends Thread {

        public void run() {
            Socket connectionSocket = null;
            DataInputStream input = null;
            ServerSocket newSocket = null;
            boolean done = false;
            long[] plotpoint = new long[1];
            try {
                newSocket = new ServerSocket(2503);
                connectionSocket = newSocket.accept();
                input = new DataInputStream(new BufferedInputStream(connectionSocket.getInputStream()));
                while (!done) {
                    try {
                        plotpoint[0] = (long) input.readInt();
                        
                        DefinedChart.addValues(System.currentTimeMillis(), plotpoint);
                        Thread.sleep(100);
                        if(plotpoint[0]>100)
                        {
                            GridBagLayoutDemo.statusToYellow();
                        }
                        else if(plotpoint[0]<=100 && plotpoint[0]>10)
                        {
                            GridBagLayoutDemo.statusToGreen();
                        }
                        else if(plotpoint[0]<=10)
                        {
                            GridBagLayoutDemo.statusToRed();
                        }
                        else
                        {
                            GridBagLayoutDemo.statusToMissing();
                        }
                    } catch (Exception e) {
                        done = true;
                    }
                }
            } catch (IOException e) {
                System.err.println("Caught IOException: "
                        + e.getMessage());
            } finally {
                try {
                    if (connectionSocket != null) {
                        connectionSocket.close();
                    }
                    if (input != null) {
                        input.close();
                    }
                    if (newSocket != null) {
                        newSocket.close();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(TabbedPaneDemo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
