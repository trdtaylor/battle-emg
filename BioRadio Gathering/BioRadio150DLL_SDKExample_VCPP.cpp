/*---------------------------------------------------------------------------
@author Travis Taylor
		Drew


C++ program to start / find / interface / list / display BioRadios
Sends double data to a socket, 8 places after the decimal and 2 places before
------------------------------------------------------------------------------*/
#include <iostream>
#include <cstdlib>
#include <conio.h>
#include <string.h>
#include <direct.h>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

//Has to be before Windows.h
#include <winsock2.h>
#include <Windows.h>
//Has to be after Windows.h
#include "BioRadio150DLL.h"

using namespace std;


void printArray (double arg[], int& length) {
  for (int n=0; n<length; n++)
    cout << arg[n] << " ";
  cout << "\n";
}


SOCKET s; // Socket Handle

/*** Error message for debugging ***/
void error(const char *msg)
{
    perror(msg);
    exit(0);
}

/*** Function to close socket connection ***/
/*** Shouldn't ever be used... program streams indefinitely! ***/
void CloseConnection()
{
	// Close the socket if it exists
	if (s)
	{
		closesocket(s);
	}
	WSACleanup(); // Clean up Winsock
}

/*** Function that creates socket and connects to server ***/
bool ConnectToHost(int PortNo, char* IPAddress)
{
	// Variables
	int error;

	// Start up Winsock?
	WSADATA wsadata;

	// Check for startup errors
	error = WSAStartup(0x0202, &wsadata);
	if (error)
	{
		return false;
	}

	// Check Winsock version
	if (wsadata.wVersion != 0x0202)
	{
		WSACleanup(); // Clean up Winsock
		return false;
	}

	// Fill out the information needed to initialize a sock
	SOCKADDR_IN target; // Socket address information
	target.sin_family = AF_INET; // Address family Internet
	target.sin_port = htons (PortNo); // Port to connect on
	target.sin_addr.s_addr = inet_addr (IPAddress); // Target IP

	// Create socket and check for errors
	s = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (s == INVALID_SOCKET)
	{
		return false; // Couldn't create the socket
	}  

	// Connect to server and check for errors
	if (connect(s, (SOCKADDR *)&target, sizeof(target)) == SOCKET_ERROR)
	{
		return false; // Couldn't connect
	}
	else
	{
		return true; // Success
	}
}

void packetMaker (char packet[], double arg[], int& length)
{
		ostringstream strs;
	for (int n=0; n<length; n++)
	{

		strs << arg[n] << ",";
		
	}
	string str = strs.str();
	cout << str << endl;

}


int main(int argc, char* argv[])
{
	//Initialize connection to Java frontend.
	string TERMINATE = "T\n";
	int portNo = 2503;
	char ipAddr[20] = "127.0.0.1";
	ConnectToHost(portNo,ipAddr);




	/* set up path to example config file */
        char *configFileName = "\\EEGConfig.ini";
        char* fullConfigFilePath = new char[100];
        _getcwd(fullConfigFilePath, 100);
        strcat(fullConfigFilePath, configFileName);
		/*
		//Save current config from bioradio.  Useful to figure out whats going on
		char *configFileName1 = "\\SavedConfig.ini";
        char* saveConfigFilePath = new char[100];
        _getcwd(saveConfigFilePath, 100);
        strcat(saveConfigFilePath, configFileName1);
		*/
		
		/*
		* Important for the size of buffers.  We don't need SLOW_INPUT_BUFFER_SIZE, and with no buffer we don't need FAST_INPUTS_BUFFER_SIZE.
		* ACQUISITION_INTERVAL correspondsd to TrnsferBufferh
		*/
        const int FAST_INPUTS_BUFFER_SIZE = 20;
        const int SLOW_INPUTS_BUFFER_SIZE = 1;
        const int ACQUISITION_INTERVAL = 20; // (milliseconds)
		bool NORMAL = true;

        /* FIND DEVICES */
        const int MAX_DEVICES = 10;
        TUpdateStatusEvent UpdateStatusFunc = NULL;
        char *portName;
        int deviceCount = 0;
        TDeviceInfo DeviceList[MAX_DEVICES];
        cout << "Searching for BioRadio 150 Computer Units..." << endl;
        FindDevices(DeviceList, &deviceCount, MAX_DEVICES, UpdateStatusFunc);
        cout << "Found " << deviceCount << " BioRadio 150 Computer Units:";
        if (deviceCount > 0) {
                for (int i=0; i<deviceCount; i++) {
                        cout << ((i!=0) ? ", " : " ") << DeviceList[i].PortName;
                }
                portName = DeviceList[0].PortName;
                cout << endl << "Using " << portName << "." << endl;
        } else {
                cout << "Exiting." << endl;
                system("pause");
                return -1;
        }

        unsigned long int devHandle;

        /* Create the BioRadio handle.  Pass a value of true if you are using a legacy Computer Unit to connect to
               your BioRadio.  Otherwise, pass false */
        devHandle = CreateBioRadio(false);
        cout << endl << "Create BioRadio Object... (" << devHandle << ")" << endl;

		/*
		* Set value for what illegal values to drop as bad
		*/
        SetBadDataValues(devHandle, -65535, 65535);


        if (devHandle>0) {
                /* START COMMUNICATION */
                int successFlag = StartCommunication(devHandle, portName);
                cout << "Start Comm... (" << successFlag << ")" << endl << endl;
                if (successFlag > 0) {
                        /* START ACQUISITION */
						/*
						* Disables the buffer, transfer immediately.  Must be done right before StartAcq
						*/
						//DisableBuffering(devHandle);
                        successFlag = StartAcq(devHandle, 1);
                        cout << "Start Acq... (" << successFlag << ")" << endl;
                        if (successFlag > 0) {
                                /* PROGRAM CONFIGURATION */
							//	SaveConfig(devHandle, saveConfigFilePath);
                               successFlag = ProgramConfig(devHandle, 1, fullConfigFilePath);
							                              
                                cout << "Program Config... (" << successFlag << ")\n" << endl;
                                if (successFlag > 0) {
                                        int numFastMainInputs, numFastAuxInputs, numSlowInputs;
                                        int numTotalInputs = GetNumEnabledInputs(devHandle,
                                                             &numFastMainInputs, &numFastAuxInputs, &numSlowInputs);
                                        cout << "Inputs: " << numFastMainInputs << " fast main, " << numFastAuxInputs <<
                                             " fast aux, " << numSlowInputs << " slow, " << numTotalInputs << " total" << endl;
                                        cout << "Sample Rate = " << GetSampleRate(devHandle) << endl;

                                        double FastInputsData[FAST_INPUTS_BUFFER_SIZE];
                                        WORD SlowInputsData[SLOW_INPUTS_BUFFER_SIZE];
                                        int FastInputsNumRead, SlowInputsNumRead, readReturn;
                                        int statusCount = 0;

										//Start of retrieval loop
                                        while (!_kbhit()) {
                                                /* REQUEST DATA EVERY 50MS UNTIL KEY PRESSED */
                                                cout << endl << "Tfr Buffer... (" << TransferBuffer(devHandle) << ")" << endl;
                                                readReturn = ReadScaledFastAndSlowData(devHandle,
                                                                                       FastInputsData, FAST_INPUTS_BUFFER_SIZE, &FastInputsNumRead,
                                                                                       SlowInputsData, SLOW_INPUTS_BUFFER_SIZE, &SlowInputsNumRead);
                                                cout << "Read Data... (" << readReturn << ")" << endl;
												cout << "Print Fast Input Buffer... (";
												printArray(FastInputsData, FastInputsNumRead);
													//while(1){send (s, buffer, 20, 0);}
												//packetMaker(buffer, FastInputsData, FastInputsNumRead) ;
												
												if(FastInputsNumRead > 0)
												{
													//We received input from device.  Add to analyization




													//Send to java application.  Current format "-1.0323, -1.3243"
													ostringstream strs;
													strs.precision(6);
													//strs.fixed;
												//	char buffer[2000];
													for (int n=0; n<FastInputsNumRead; n++)
													{
															if(FastInputsData[n]!=(-65535))
															{
																strs << fixed << (signed int)(FastInputsData[n]*100);
																strs << ",";
															}
															else
															{
																n=FastInputsNumRead;
																	NORMAL = false;

															}

														//Testing new string method
														//char * test = (char *) &FastInputsData[n];
												//		buffer = _fcvt(FastInputsNumRead[n], 7, NULL, NULL);
												//		cout << "New String Test: (" <<  << ")" << endl;
												//		cout << flush;
												//		sprintf(buffer,  "%g,", FastInputsData[n]);
													}
													if(NORMAL){
													string str = strs.str();

											//Removes last comma from the message to server
													str.pop_back();
													str.append("\n");

													//Testing of the TestBuffer
											//	cout << "TestBuffer: (" <<  buffer  << ")" << endl;
											//	cout << flush;
													cout << "Sending... " << endl;
													cout << str << endl;
													send (s, str.c_str(), str.size(), 0);
													cout << "Size of c_str: " << str.size() << endl;
													cout << "size of str: " << (sizeof(str)) << endl;

												}
												

												/*
                                                cout << "Fast Inputs Num Read = " << FastInputsNumRead << endl;
                                                cout << "Slow Inputs Num Read = " << SlowInputsNumRead << endl;
                                                if (statusCount++ >= 20) {
                                                        statusCount = 0;
                                                        cout << endl;
                                                        cout << "  BatteryStatus = " << GetBatteryStatus(devHandle) << endl;
                                                        cout << "  RSSI: " << GetUpRSSI(devHandle) << " up / " << GetDownRSSI(devHandle) << " down" << endl;
                                                        cout << "  BufSz: " << GetLinkBufferSize(devHandle) << endl;
                                                        cout << "  BEC: " << GetBitErrCount(devHandle) << endl;
                                                        cout << "  BER: " << GetBitErrRate(devHandle) << endl;
                                                        cout << "  Pkts: " << GetGoodPackets(devHandle) << " good " <<
                                                             GetBadPackets(devHandle) << " bad " <<
                                                             GetDroppedPackets(devHandle) << " dropped" << endl;
                                            
												}
												    */
												}
												else
												{
													NORMAL = true;
												}
                                                Sleep(ACQUISITION_INTERVAL);
                                        }
                                }
                                /* STOP ACQUISITION */
                                cout << endl << "Stop Acq... (" << StopAcq(devHandle) << ")" <<  endl;
                        }
                        /* STOP COMMUNICATION */
                        cout << endl << "Stop Comm... (" << StopCommunication(devHandle) << ")" << endl;
                }
                successFlag = DestroyBioRadio(devHandle);
                cout << "Destroy BioRadio Object... (" << successFlag << ")\n" << endl;
        }
		//Send signal to server to terminate
		for(int i=0; i<10; i++)
		{
			cout << "Sending: (" << TERMINATE << " )" ;
			
			send(s, TERMINATE.c_str(), sizeof(TERMINATE.c_str()), 0);
			
		}
		CloseConnection();
        system("pause");
        return 1;
}


//---------------------------------------------------------------------------
